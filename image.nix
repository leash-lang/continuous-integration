# from https://github.com/LnL7/nix-docker/blob/master/default.nix
{ name ? "leash-ci"
, tag ? "latest"
, entrypoint ? [ ]
, contents ? [ ]
, bashInteractive
, buildEnv
, cacert
, coreutils
, dockerTools
, nixUnstable
, pkgs
, shadow
, stdenv
, lib
, nixpkgs
, system
, openssl
, makeWrapper
}:
let
  inherit (lib) concatStringsSep genList;

  shadow = pkgs.shadow.override {
    pam = null;
  };

  path = buildEnv {
    name = "system-path";
    paths = [ bashInteractive coreutils nixUnstable shadow ];
  };

  nixconf = ''
    build-users-group = nixbld
    sandbox = true
    experimental-features = nix-command flakes
  '';

  passwd = ''
    root:x:0:0::/root:/run/current-system/sw/bin/bash
    ${concatStringsSep "\n" (genList (i: "nixbld${toString (i + 1)}:x:${toString (i + 30001)}:30000::/var/empty:/run/current-system/sw/bin/nologin") 32)}
  '';

  group = ''
    root:x:0:
    nogroup:x:65534:
    nixbld:x:30000:${concatStringsSep "," (genList (i: "nixbld${toString (i + 1)}") 32)}
  '';

  nsswitch = ''
    hosts: files dns myhostname mymachines
  '';

  sysContents = stdenv.mkDerivation {
    name = "user-environment";
    phases = [ "installPhase" "fixupPhase" ];
    nativeBuildInputs = [ makeWrapper ];

    exportReferencesGraph =
      map (drv: [ ("closure-" + baseNameOf drv) drv ]) [ path cacert nixpkgs openssl ];

    installPhase = ''
      mkdir -p $out/run/current-system $out/var
      ln -s /run $out/var/run
      ln -s ${path} $out/run/current-system/sw
      mkdir -p $out/bin $out/usr/bin $out/sbin
      ln -s ${stdenv.shell} $out/bin/sh
      ln -s ${coreutils}/bin/env $out/usr/bin/env
      mkdir -p $out/etc/nix
      echo '${nixconf}' > $out/etc/nix/nix.conf
      echo '${passwd}' > $out/etc/passwd
      echo '${group}' > $out/etc/group
      echo '${nsswitch}' > $out/etc/nsswitch.conf
      mkdir -p $out/etc/ssl/certs
      ln -s ${cacert}/etc/ssl/certs/ca-bundle.crt $out/etc/ssl/certs/ca-bundle.crt
      mkdir -p $out/tmp
      mkdir -p $out/bin
      makeWrapper ${nixUnstable}/bin/nix $out/bin/nix --add-flags --print-build-logs
      printRegistration=1 ${pkgs.perl}/bin/perl ${pkgs.pathsFromGraph} closure-* > $out/.reginfo
    '';
  };

in
dockerTools.buildImage {
  inherit name tag;
  contents = [ sysContents ] ++ contents;
  config.Cmd = [ "${bashInteractive}/bin/bash" ];
  config.Entrypoint = [ entrypoint ];
  config.Env =
    [
      "USER=root"
      "PATH=/bin:/sbin:/root/.nix-profile/bin:/run/current-system/sw/bin:/usr/bin"
      "MANPATH=/root/.nix-profile/share/man:/run/current-system/sw/share/man"
      "NIX_PAGER=cat"
      "NIX_PATH=nixpkgs=${nixpkgs}"
      "NIX_SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt"
    ];
}
