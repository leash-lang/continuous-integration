{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/21.11";
    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
    pre-commit-hooks.inputs.nixpkgs.follows = "/nixpkgs";
    pre-commit-hooks.inputs.flake-utils.follows = "/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        inherit (builtins) readFile;

        name = "leash-ci";
        pkgs = nixpkgs.legacyPackages.${system};

        cachix-watch = pkgs.writeScriptBin "cachix-watch" (readFile ./bin/cachix-watch);

        entrypoint = pkgs.writeScript "init" ''
          #!${pkgs.bashInteractive}/bin/bash
          #
          # Cachix is a b*tch to install, this is what we have to do for now
          #
          nix-env -iA cachix -f https://cachix.org/api/v1/install

          #
          # Gitlab should set up these variables, if not then just ignore and
          # move on.
          #
          if [[ -n "''${CACHIX_AUTH_TOKEN}" ]]; then
            if [[ -n "''${CACHIX_CACHE}" ]]; then
              cachix use "''${CACHIX_CACHE}"
              cachix use pre-commit-hooks
            else
              >&2 echo \$CACHIX_CACHE not set!
            fi
          else
            >&2 echo \$CACHIX_AUTH_TOKEN not set!
          fi

          exec "$@"
        '';

        haskellPackages = pkgs.haskell.packages.ghc8107;

        baseImageContents = with pkgs; [
          cachix-watch
          findutils
          git
          gnugrep
          gnused
          zlib
        ];

        ghcImageContents = [
          haskellPackages.ghc
          haskellPackages.cabal-install
        ];

      in
      rec {
        inherit haskellPackages;

        defaultPackage = self.packages.${system}.baseImage;

        packages = rec {
          baseImageDeps = pkgs.symlinkJoin {
            name = "base-image-deps";
            paths = baseImageContents;
          };

          baseImage = pkgs.callPackage ./image.nix {
            inherit nixpkgs entrypoint;
            name = "base-image";
            tag = "latest";
            contents = baseImageContents;
          };

          ghcImageDeps = pkgs.symlinkJoin {
            name = "haskellPackages-image-deps";
            paths = ghcImageContents;
          };

          ghcImage = pkgs.dockerTools.buildImage {
            name = "haskellPackages";
            tag = "latest";
            fromImage = baseImage;
            config.Entrypoint = entrypoint;
            config.Cmd = [ "${pkgs.bashInteractive}/bin/bash" ];
            contents = ghcImageContents;
          };
        };
      });
}
